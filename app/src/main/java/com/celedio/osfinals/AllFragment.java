package com.celedio.osfinals;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class AllFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_all, container, false);

        ArrayList<Places> places = new ArrayList<>();
        String[] descShort = getResources().getStringArray(R.array.places_short_desc);
        String[] descLong = getResources().getStringArray(R.array.places_long_desc);
        RecyclerView mRecyclerView = view.findViewById(R.id.mRecyclerView);

        places.add(new Places(R.drawable.baguio_city, R.drawable.baguio_city_map, "Baguio City", descShort[0], descLong[0]));
        places.add(new Places(R.drawable.burnham_park, R.drawable.burnham_park_map, "Burnham Park", descShort[1], descLong[1]));
        places.add(new Places(R.drawable.la_trinidad, R.drawable. la_trinidad_map, "La Trinindad",  descShort[2], descLong[2]));
        places.add(new Places(R.drawable.colors_of_stobosa, R.drawable.colors_of_stobosa_map, "Colors of Stobosa",  descShort[3], descLong[3]));

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(places, mRecyclerView.getContext());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);

        return view;
    }
}