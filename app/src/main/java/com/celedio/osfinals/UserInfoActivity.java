package com.celedio.osfinals;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;

import java.util.Calendar;
import java.util.HashMap;

public class UserInfoActivity extends AppCompatActivity implements RadioGroup.OnCheckedChangeListener {

    DatabaseReference userRef;
    FirebaseAuth mAuth;

    ProgressDialog loadingBar;
    DatePickerDialog datePickerDialog;
    Button birthdateButton;
    Button submitButton;
    EditText name, address;
    RadioGroup radioGroup;
    RadioButton radioButton;
    String gender;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_userinfo);

        mAuth = FirebaseAuth.getInstance();
        String currentUserID = mAuth.getCurrentUser().getUid();
        userRef = FirebaseDatabase.getInstance().getReference().child("USERS").child(currentUserID);

        name = findViewById(R.id.FullName);
        address = findViewById(R.id.HomeAddress);
        loadingBar = new ProgressDialog(this);

        DatePickerInitialize();
        birthdateButton = findViewById(R.id.BirthdateButton);

        radioGroup = (RadioGroup) findViewById(R.id.GenderGroup);


        submitButton = findViewById(R.id.SubmitButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SaveUserInfo();
            }
        });
    }


    private void DatePickerInitialize() {
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                month = month + 1;
                String date = makeDateString(dayOfMonth,month,year);
                birthdateButton.setText(date);
            }
        };
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);

        int style = AlertDialog.THEME_DEVICE_DEFAULT_LIGHT;

        datePickerDialog = new DatePickerDialog(this, style, dateSetListener,year,month,dayOfMonth);
        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
    }

    private String makeDateString(int day, int month, int year) {
        return getMonthFormat(month) + " " + day + " " + year;
    }

    private String getMonthFormat(int month)
    {
        if(month == 1)
            return "JAN";
        if(month == 2)
            return "FEB";
        if(month == 3)
            return "MAR";
        if(month == 4)
            return "APR";
        if(month == 5)
            return "MAY";
        if(month == 6)
            return "JUN";
        if(month == 7)
            return "JUL";
        if(month == 8)
            return "AUG";
        if(month == 9)
            return "SEP";
        if(month == 10)
            return "OCT";
        if(month == 11)
            return "NOV";
        if(month == 12)
            return "DEC";

        return "JAN";
    }

    private void SaveUserInfo() {
        String fullName = name.getText().toString();
        String homeAddress = address.getText().toString();
        String birthDate = birthdateButton.getText().toString();
        int selectedID = radioGroup.getCheckedRadioButtonId();
        radioButton = (RadioButton) findViewById(selectedID);
        String sex = radioButton.getText().toString();

        if (TextUtils.isEmpty(fullName)){
            Toast.makeText(this, "Invalid! Please enter your full name", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(homeAddress)){
            Toast.makeText(this, "Invalid! Please enter your home address", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(birthDate)){
            Toast.makeText(this, "Invalid! Please choose your birthdate", Toast.LENGTH_SHORT).show();
        }
        if (TextUtils.isEmpty(sex)){
            Toast.makeText(this, "Invalid! Please choose your sex", Toast.LENGTH_SHORT).show();
        }
        else {
            loadingBar.setTitle("Creating Profile");
            loadingBar.setMessage("Please wait, while we are setting things up");
            loadingBar.show();
            loadingBar.setCanceledOnTouchOutside(true);

            HashMap userMap = new HashMap();
            userMap.put("Full Name", fullName);
            userMap.put("Home Address", homeAddress);
            userMap.put("Birthdate", birthDate);
            userMap.put("Gender", gender);

            userRef.updateChildren(userMap).addOnCompleteListener(new OnCompleteListener() {
                @Override
                public void onComplete(@NonNull Task task) {
                    if(task.isSuccessful()){
                        sendUserToMainActivity();
                        Toast.makeText(UserInfoActivity.this, "Setup Complete", Toast.LENGTH_LONG).show();
                        loadingBar.dismiss();
                    } else {
                        Toast.makeText(UserInfoActivity.this, "Error Occurred: " +
                                task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        loadingBar.dismiss();
                    }
                }
            });

        }
    }
    private void sendUserToMainActivity() {
        Intent mainIntent = new Intent(UserInfoActivity.this, MainActivity.class);
        mainIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(mainIntent);
        finish();
    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {

        switch (checkedId){
            case R.id.FemaleButton:
                gender="Female";
                break;

            case R.id.MaleButton:
                gender="Male";
                break;
        }
    }

    public void openDatePicker(View view) {
        datePickerDialog.show();
    }

}
