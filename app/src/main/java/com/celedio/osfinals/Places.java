package com.celedio.osfinals;

import com.google.gson.annotations.SerializedName;

public class Places {
    @SerializedName("Image Id")
    private int cardImageUrl;
    @SerializedName("Map Image Id")
    private int cardMapImageUrl;
    @SerializedName("Title")
    private String cardTitle;
    @SerializedName("Short Description")
    private String cardDescriptionShort;
    @SerializedName("Long Description")
    private String cardDescriptionLong;

    public Places(int cardImageUrl, int cardMapImageUrl, String cardTitle, String cardDescriptionShort, String cardDescriptionLong){
        this.cardImageUrl = cardImageUrl;
        this.cardMapImageUrl = cardMapImageUrl;
        this.cardTitle = cardTitle;
        this.cardDescriptionShort = cardDescriptionShort;
        this.cardDescriptionLong = cardDescriptionLong;
    }

    public int getCardImageUrl() {
        return cardImageUrl;
    }

    public int getCardMapImageUrl() {
        return cardMapImageUrl;
    }

    public String getCardTitle() {
        return cardTitle;
    }

    public String getCardDescriptionShort() {
        return cardDescriptionShort;
    }

    public String getCardDescriptionLong() {
        return cardDescriptionLong;
    }

    public void setCardImageUrl(int cardImageUrl) {
        this.cardImageUrl = cardImageUrl;
    }

    public void setCardMapImageUrl(int cardMapImageUrl) {
        this.cardMapImageUrl = cardMapImageUrl;
    }

    public void setCardTitle(String cardTitle) {
        this.cardTitle = cardTitle;
    }

    public void setCardDescriptionShort(String cardDescriptionShort) {
        this.cardDescriptionShort = cardDescriptionShort;
    }

    public void setCardDescriptionLong(String cardDescriptionLong) {
        this.cardDescriptionLong = cardDescriptionLong;
    }
}

