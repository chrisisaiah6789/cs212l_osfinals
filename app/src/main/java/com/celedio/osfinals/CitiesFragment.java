package com.celedio.osfinals;

import android.annotation.SuppressLint;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;


public class CitiesFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cities, container, false);

        ArrayList<Places> places = new ArrayList<>();
        String[] descShort = getResources().getStringArray(R.array.places_short_desc);
        String[] descLong = getResources().getStringArray(R.array.places_long_desc);
        @SuppressLint({"MissingInflatedId", "LocalSuppress"}) RecyclerView mRecyclerView = view.findViewById(R.id.mRecyclerView);

        places.add(new Places(R.drawable.baguio_city, R.drawable.baguio_city_map, "Baguio City", descShort[0], descLong[0]));
        places.add(new Places(R.drawable.la_trinidad, R.drawable. la_trinidad_map, "La Trinindad",  descShort[2], descLong[2]));

        RecyclerViewAdapter adapter = new RecyclerViewAdapter(places, mRecyclerView.getContext());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(view.getContext(), LinearLayoutManager.VERTICAL, false);

        mRecyclerView.setLayoutManager(linearLayoutManager);
        mRecyclerView.setAdapter(adapter);

        return view;
    }
}