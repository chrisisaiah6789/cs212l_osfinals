package com.celedio.osfinals;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public static class ViewHolder extends RecyclerView.ViewHolder{
        private final TextView placesTitle;
        private final TextView placesDesc;
        private final ImageView placesImage;

        public ViewHolder(View itemView){
            super(itemView);

            placesTitle = itemView.findViewById(R.id.card_item_title);
            placesDesc = itemView.findViewById(R.id.card_item_description);
            placesImage = itemView.findViewById(R.id.card_item_image);
        }
    }

    private final ArrayList<Places> mPlaces;
    private final Context mContext;

    public RecyclerViewAdapter(ArrayList<Places> places, Context context){
        this.mPlaces = places;
        this.mContext = context;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View placesView = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_model,parent, false);
        return new ViewHolder(placesView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Places places = mPlaces.get(position);
        holder.placesTitle.setText(places.getCardTitle());
        holder.placesDesc.setText(places.getCardDescriptionShort());
        holder.placesImage.setImageResource(places.getCardImageUrl());

        holder.itemView.setOnClickListener((view) -> {
            Intent intent = new Intent(mContext, PlacesFragment.class);
            intent.putExtra("image_url", places.getCardMapImageUrl());
            intent.putExtra("place_title", places.getCardTitle());
            intent.putExtra("place_desc", places.getCardDescriptionLong());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }
}
