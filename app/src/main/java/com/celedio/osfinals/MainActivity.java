package com.celedio.osfinals;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.celedio.osfinals.databinding.ActivityMainBinding;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationBarView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


public class MainActivity extends AppCompatActivity {

    ActivityMainBinding bind;

    private FirebaseAuth mAuth;
    private DatabaseReference UserRef;

    AllFragment allFragment = new AllFragment();
    CitiesFragment citiesFragment = new CitiesFragment();
    AttractionsFragment attractionsFragment = new AttractionsFragment();
    EditText editText;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bind=ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(bind.getRoot());

        BottomNavigationView navBar = findViewById(R.id.NavBar);
        getSupportFragmentManager().beginTransaction().replace(R.id.Container,allFragment).commit();

        navBar.setOnItemSelectedListener(new NavigationBarView.OnItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.navigation_all:
                        getSupportFragmentManager().beginTransaction().replace(R.id.Container,allFragment).commit();
                        return true;
                    case R.id.navigation_cities:
                        getSupportFragmentManager().beginTransaction().replace(R.id.Container,citiesFragment).commit();
                        return true;
                    case R.id.navigation_attractions:
                        getSupportFragmentManager().beginTransaction().replace(R.id.Container,attractionsFragment).commit();
                        return true;
                }
                return false;
            }
        });

        mAuth = FirebaseAuth.getInstance();
        UserRef = FirebaseDatabase.getInstance().getReference().child("USER");
    }


    protected void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        //means that the user is not authenticated
        if (currentUser == null){
            sendUserToLoginActivity();
        }
        else {
            CheckUserExistence();
        }
    }

    private void SendUserToSetupActivity() {
        Intent setupIntent = new Intent(MainActivity.this,UserInfoActivity.class);
        setupIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(setupIntent);
        finish();
    }

    private void sendUserToLoginActivity(){
        Intent loginIntent = new Intent(MainActivity.this,LoginActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(loginIntent);
        finish();
    }

    private void CheckUserExistence() {
        final String current_user_id = mAuth.getCurrentUser().getUid();

        UserRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                if(!snapshot.hasChild(current_user_id)){
                    SendUserToSetupActivity();
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
}