package com.celedio.osfinals;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

public class PlacesFragment extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_places);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationOnClickListener((view) -> {
            Intent intent = new Intent(this, MainActivity.class);
            this.startActivity(intent);
        });

        getIncomingIntent();
    }

    private void getIncomingIntent(){
        if (getIntent().hasExtra("image_url") && getIntent().hasExtra("place_title") && getIntent().hasExtra("place_desc")){
            int imageUrl = getIntent().getIntExtra("image_url", 0);
            String placesTitle = getIntent().getStringExtra("place_title");
            String placesDesc = getIntent().getStringExtra("place_desc");

            setContent(imageUrl,placesTitle,placesDesc);
        }
    }

    private void setContent(int imageUrl, String placesTitle, String placesDesc){
        TextView title = findViewById(R.id.place_title);
        title.setText(placesTitle);
        TextView desc = findViewById(R.id.place_description);
        desc.setText(placesDesc);
        ImageView image = findViewById(R.id.map_image);
        image.setImageResource(imageUrl);
    }
}
